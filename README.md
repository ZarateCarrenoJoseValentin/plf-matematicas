#  <img src="Images/logo.jpg" width="70" height="70"/> INSTITUTO TECNOLÓGICO DE OAXACA
## PROGRAMACION LOGICA Y FUNCIONAL - SCC1019 - ISB 


### Mapa Conceptual. Conjuntos, Aplicaciones y funciones (2002).
```plantuml

@startmindmap
skinparam backgroundColor #fff
skinparam sequenceMessageAlign center
skinparam shadowing true
skinparam component {
FontName Arial
FontSize 17
}
<style>
mindmapDiagram{
    node{
        BackGroundColor  #0FD8DB
    }
}
</style>

skinparam title {
FontName Arial 
FontSize 40
Fontcolor #fff
FontStyle bold
BackgroundColor #000
ArrowFontColor #000 
}
title "Teoría de conjuntos, aplicaciones y funciones "

*[#F0C825] Teoría de conjuntos,\naplicaciones y funciones
**_ es\nun
*** Conjunto
****_ es una
***** Colección de objetos,\nllamados elementos
******_ consta\nde

******* Características 
********_ son
********* El conjunto esta\n ligado a los elementos
********* Tienen una relación de pertenencia entre sí
********* Siempre sabes si el elemento está en el conjunto

******* Tipos
********_ se\nclasifican en

********* Universal
**********_ es\nun
*********** Conjunto de referencia\ndonde tiene lugar\ntoda la teoría 
************_ sus
************* Características
**************_ son
*************** Cada grupo A es un subgrupo de U
*************** Unión del grupo A\ncon el grupo U es igual a U

********* Vacio
**********_ es\nuna
*********** Necesidad lógica de\ncerrar una matriz sin elementos
************_ sus
************* Características
**************_ son
*************** No tiene ningún elemento
*************** Es único

******* Formula
********_ ¿Qué son?
*********  Cardinal de la unión de grupos
********* Acotacion de cardinales de grupos

******* Cardinalidad de\nun conjunto
********_ ¿Qué es?
********* Número de elementos\nque componen el conjunto. 


******* Diagramas de\n Venn 
********_ creador
********* John Venn 
********_ ¿Qué son?
********* Esquema utilizado\nen teoría de conjuntos

********_ ¿Para que se\nutilizan?
********* Mostrar la agrupación\nde elementos en conjuntos
**********_ sus
*********** Características
************_ son
*************  Diagrama\nde conjuntos
*************  Su campo de estudio es la estadística
*************  Representado por círculos

******* Operaciones
********_ son
********* intersección
********* Unión
********* Complementación
********* Implementacion

******* Otros elementos
********_ estos son
********* Imagen
**********_ ¿Qué es?
*********** Campo de valores o rango
********* Imagen inversa
**********_ ¿Qué es?
*********** Un grupo lo hace coincidir con otro grupo

**_ sus
*** Areas de\nestudio
****_ son
***** Teorias 
******_ se\nclasifican
******* Conjuntos\ncombinatoria
********_ ¿Qué es?
********* Conjuntos finitos
******* Descriptiva\nde conjuntos 
********_ ¿Qué estudia?
********* Los conjuntos
**********_ estos pueden\nser
*********** Recta real
*********** Espacios polacos
******* Conjuntos\nDifusos
********_ consta\nde
********* Elementos
********* Grado de pertenencia
******* Modelo Interno
********_ ¿Para que\nsirve?
********* Mostrar resultados consistentes


*** Aplicaciones y\nFunciones
****_ consta\nde
***** Aplicaciones
******_ se define\ncomo
******* Transformación o una regla

******_ tipos
******* Inyectiva 
******* Sobreyectiva
******* Biyectiva 


***** Funciones
******_ se define
******* Conjunto de puntos 
@endmindmap
```
**Fuentes:**
[Conjuntos, Aplicaciones y funciones](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

#### Mapa Conceptual. Funciones (2010).

```plantuml

@startmindmap
skinparam backgroundColor #fff
skinparam sequenceMessageAlign center
skinparam shadowing true
skinparam component {
FontName Arial
FontSize 17
}
<style>
mindmapDiagram{
    node{
        BackGroundColor  #FFC900
    }
}
</style>

skinparam title {
FontName Arial 
FontSize 40
Fontcolor #fff
FontStyle bold
BackgroundColor #000
ArrowFontColor #000 
}
title " Funciones"

*[#12E7AA] Funciones
**_ se define
*** Transformación de un\nconjunto a otro
****_ ¿Como se representan?
***** Representación Cartesiana 
******_ autor
******* René Descartes
******_ es
******* Sistema bidimencional 
********_ sus
********* Características
**********_ son
*********** Consta de dos ejes
*********** Eje de las abscisas
*********** Eje de las ordenadas
*********** Denominado plano cartesiano

**_ la
*** Familia de funciones
****_ pueden\nser
***** Funciones Algebraicas
******_ son
******* Funciones 
********_ se clasifican\nen
********* Explicitas 
********* Implicitas
********* Polinómicas
********* Racionales
********* Radicales

***** Funciones trascendentales 
******_ son 
******* Funciones
********_ se clasifican\nen
********* Exponenciales
********* Logarítmicas
********* Trigonométricas

**_ consta\nde
*** Variables
****_ se clasifican\nen
***** Dependiente 
******_ ¿Qué es?
******* Representan algún\ntipo de cantidad
********_ ejemplos
********* Consumo de cigarrillos
********* La capa de ozono
********* La temperatura

***** Independiente
******_ ¿Qué es?
*******  determina el valor de la variable\ndependiente
********_ se caracteriz\npor
********* Corresponde con el eje de las abscisas
********* Mayor variables independientes, mejor interpretación 

**_ sus
*** Características
****_ de las
***** Funciones
******_ pueden ser
******* Crecientes 
********_ se caracterizan\npor
********* Aumenta las variables independientes, su imagen aumenta
******* Decrecientes
********_ se caracterizan\npor
********* Aumenta las variables independientes, su imagen decrementa
********* En la gráfica, el histograma es decreciente
******* Intuitiva
********_ consta de
********* Máximos
**********_ se caracterizan\npor
*********** La función alcanza un\ncierto valor
************_ entonces
************* La función comienza\na declinar
********* Mínimos 
**********_ se caracterizan\npor
*********** La función disminuye\na un cierto valor
************_ entonces
*************  La función comienza\na aumentar

**_ otras
*** ideas

**** Derivada
*****_ ¿Qué es?
******  Resolver un problema de aproximación de una función compleja 
*****_ sus
****** Características
*******_ son
******** Aproximación al cambio
******** Solución mediante una función lineal
******** Mide el cambio


**** Limite 
*****_ ¿Qué es?
****** Valor que estamos tratando desde el punto de partida

**** Continuidad  
*****_ ¿Qué es?
****** Esta función no genera saltos

@endmindmap
```
**Fuentes:**
[Funciones](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)

### Mapa Conceptual. La matemática del computador (2002).

```plantuml

@startmindmap
skinparam backgroundColor #fff
skinparam sequenceMessageAlign center
skinparam shadowing true
skinparam component {
FontName Arial
FontSize 17
}
<style>
mindmapDiagram{
    node{
        BackGroundColor  #11CA9D
    }
}
</style>

skinparam title {
FontName Arial 
FontSize 40
Fontcolor #fff
FontStyle bold
BackgroundColor #000
ArrowFontColor #000 
}
title " La matemática del computador "

*[#4F8AFF] La matemática del\ncomputador
**_ la 
*** Base del funcionamiento
****_ son
***** Las Matemáticas


*** Aritmetica Finita
****_ ¿Qué es?
***** serie aritmetica
******_ consiste 
******* suma de los términos\nequidistantes
********_ de los
********* extremos es igual a la\nsuma de los extremos

****_ sus
***** Características
******_ son
******* No puedes representar números infinitos
******* Consta de digitos significativos 


*** Aritmética del computador
****_ contiene
***** Números 
******_ sus
******* Propiedades
********_ son

********* Fraccionales 
**********_ son
*********** División de números naturales 
**********_ ejemplos
*********** 1/3

********* Naturales
**********_ son
*********** Números enteros positivos
**********_ ejemplos
*********** N = {0,1,2, 3..}


********* Decimales
**********_ son
*********** Representan con una coma y que tienen una parte entera
**********_ ejemplos
*********** 0,20 


********* Truncar un número 
**********_ son
*********** Quitar los números a la derecha de la unidad
**********_ ejemplos
*********** a = 2,654 -> a = 2,65 


********* Redondeo
**********_ es
*********** Proceso para modificar un número
**********_ ejemplos
***********  a = 1,275 -> a = 1,28


**_ el
*** Sistema Binario 
****_ ¿Qué es?
***** Representación 
******_ utiliza
******* Números: 0 y 1

****_ sus
***** Características
******_ son
******* Utiliza únicamente dos dígitos {0,1}
******* Cada número tiene un valor diferente según la posicion
******* Se utilizan en todo tipo de ordenadores

****_ su
***** Aplicación
******_ ejemplos
******* Programación
******* Cifrado de información confidencial
******* Transferencia de datos
******* Protocolos de comunicación

*** Sistema Octal
****_ ¿Qué es?
***** sistema de numeración en base 8
******_ utiliza
******* Digitos 0,7
****_ sus
***** Ventajas
******_ son
******* Un tercio de longitud del binario
******* Fácil conversión de binario a octal y viceversa
******* Fácil de manejar entrada y salida en la forma octal

***** Desventajas
******_ son
******* El ordenador no entiende el sistema octal
******* Dificil diseñar circuitos electronicos




@endmindmap
```

**Fuentes:**
1. [La matemática del computador. El examen final](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)
2. [Las matemáticas de los computadores. El examen final](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)

### Estudiante ✒️
Zarate Carreño José Valentín